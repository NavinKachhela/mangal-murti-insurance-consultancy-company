$('.carousel').carousel({
    interval: 4000
});
$('.counter').counterUp({
    delay: 10,
    time: 3000,
    triggerOnce: true
});
new WOW().init();
